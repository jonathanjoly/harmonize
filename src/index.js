var m = require("mithril");



var Header = require("./views/header");
var Content = require("./views/content");
var Footer = require("./views/footer");


var App = {
  view: function() {
    // Create a DIV.container
    return m('.page', [
		m(Header,{}),
		m(Content,{}),
		m(Footer,{})
    ]);
  }
}


m.mount(document.body, App);
