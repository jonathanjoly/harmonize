var m = require("mithril")


var Rules = {
	notes :['do','re','mi','fa','sol','la','si'],
	natureAccords:{
		'Majeur':{
			'son3':['','m','m','','','m','°'],
			'son4':['▲','m 7','m 7','▲','7','m 7','Ø']
		},
		'mineur':{
			'naturelle':{
				'son3':['m','°','','m','m','',''],
				'son4':['m 7','Ø','▲','m 7','m 7','▲','7']
			},
			'harmonique':{
				'son3':['m','°','+','m','','','°'],
				'son4':['m ▲','Ø','+ ▲','m 7','7','▲','°']
			},
			'melodique':{
				'son3':['m','m','+','','','°','°'],
				'son4':['m ▲','m 7','+ ▲','7','7','Ø','Ø']
			},
		}
	},


	getListForGamme : function(gamme){
		var before = [];
		var after = [];
		var noteFound = false;

		for(var i=0;i<this.notes.length;i++){

			if(this.notes[i] === gamme){
				noteFound = true;
			}

			if(noteFound){
				after.push(this.notes[i]);
			}
			else{
				before.push(this.notes[i]);
			}

		}

		return after.concat(before);
	},
	harmonize : function(gamme){
		var nom  = gamme.nom.toLowerCase().replace('#','').replace('b','');

		var harmonization = {};

		var listOfNotes = this.getListForGamme(nom);
		listOfNotes= this.alteration(listOfNotes,gamme.notesAltere,gamme.alteration);


		if(gamme.tonalite === 'mineur'){
			harmonization.naturelle = {};
			harmonization.naturelle.son3 = this.accord(listOfNotes,gamme.tonalite,'son3','naturelle');
			harmonization.naturelle.son4 = this.accord(listOfNotes,gamme.tonalite,'son4','naturelle');

			var listOfNotesHarmonique = listOfNotes;
			listOfNotesHarmonique[6] = this.augmenter(listOfNotesHarmonique[6]);
			harmonization.harmonique={}
			harmonization.harmonique.son3 = this.accord(listOfNotesHarmonique,gamme.tonalite,'son3','harmonique');
			harmonization.harmonique.son4 = this.accord(listOfNotesHarmonique,gamme.tonalite,'son4','harmonique');

			var listOfNotesMelodique = listOfNotesHarmonique;
			listOfNotesMelodique[5] = this.augmenter(listOfNotesMelodique[5]);
			harmonization.melodique={}
			harmonization.melodique.son3 = this.accord(listOfNotesMelodique,gamme.tonalite,'son3','melodique');
			harmonization.melodique.son4 = this.accord(listOfNotesMelodique,gamme.tonalite,'son4','melodique');

		}
		else{
			harmonization.son3 = this.accord(listOfNotes,gamme.tonalite,'son3');
			harmonization.son4 = this.accord(listOfNotes,gamme.tonalite,'son4');
		}

		return harmonization;

	},
	alteration:function(listOfNotes,noteAlterer,alteration){
		for(var i=0;i<noteAlterer.length;i++){
			for(var j=0;j<listOfNotes.length;j++){
				if(noteAlterer[i] === listOfNotes[j]){
					listOfNotes[j]+=alteration;
				}
			}
		}
		return listOfNotes;
	},
	accord: function(listOfNotes,tonalite,sons,type){
		var list = [];
		var accords = [];

		if(tonalite === 'mineur'){
			 accords = this.natureAccords[tonalite][type][sons];
		}
		else{
			 accords = this.natureAccords[tonalite][sons];
		}


		for(var i=0;i<accords.length;i++){
			list.push(listOfNotes[i]+' '+accords[i]);
		}
		return list;
	},
	augmenter:function(note){
		var noteRes = note;
		var indexDiese = note.indexOf('#');
		var indexBemol = note.indexOf('b');

		if(-1 < indexDiese){
			noteRes+='#';
		}
		else if(-1 < indexBemol){
			noteRes = noteRes.replace('b','');
		}
		else{
			noteRes+='#';
		}

		return noteRes;
	}

	/*alteration
:
"b"
id
:
"SibMajeur"
nom
:
"Sib"
notesAltere
:
Array[2]
tonRelatif
:
"Sol"
tonalite
:
"Majeur"*/


}


module.exports = Rules
