var m = require("mithril")
var Rules = require("./Rules")

var Gammes = {
	current: {},
	harmony:{},
	relativeGamme:{},
	relativeHarmony:{},
	list: [],
	setCurrent: function(id){
		Gammes.current = Gammes.get(id);

		var relativeId = Gammes.current.tonRelatif;

		if(Gammes.current.tonalite === 'Majeur'){
			relativeId+='mineur';
		}
		else{
			relativeId+='Majeur';
		}
		Gammes.relativeGamme = Gammes.get(relativeId);
		console.log(Gammes.relativeGamme)
	},
	get: function(id){
		var gamme = {};
		for(var i=0; i< Gammes.list.length;i++){
			if(Gammes.list[i].id === id){
			 gamme = Gammes.list[i];
			}
		}
		return gamme;
	},

 loadList: function() {
	return m.request({
		method: "GET",
		url: "data.json",

	})
	.then(function(result) {
		Gammes.list = result;
	})
 },
 harmonize: function(){
	 Gammes.harmony= Rules.harmonize(Gammes.current);
	 Gammes.relativeHarmony =Rules.harmonize(Gammes.relativeGamme);
 }

}

module.exports = Gammes
