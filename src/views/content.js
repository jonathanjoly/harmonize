var m = require("mithril");

var GammeSelect = require("./gammeSelect");
var Harmonization = require("./harmonization");
var Help = require("./help");

module.exports = {
    view: function() {
        return m('.content',[
            m('.top-content',m(GammeSelect,{})),
            m('.bottom-content',[
                m('.bottom-left-content',m(Harmonization,{})),
                m('.bottom-right-content',m(Help,{}))
            ])
        ]);
    }
};
