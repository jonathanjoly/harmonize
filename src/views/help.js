var m = require("mithril");
var Gammes = require("../models/gammes")

module.exports = {
    view: function() {

        var helpLi = [];
        var help =[];

        if(Object.keys(Gammes.current).length > 0){
            helpLi = [
                m('li','Accords majeurs 7 (M7 ou ▲)'),
                m('li','Accords 7 (7)'),
                m('li','Accords mineurs 7 (m7 ou -7)'),
                m('li','Accords mineurs 7/5b (m7/5b ou Ø)'),
                m('li','Accords diminués (dim ou °)')
            ];
            help = m('.panel',[
                m('h1','Help'),
                m('ul.help-list',helpLi)
            ]);
        }

        return m('',help);
    }
};
