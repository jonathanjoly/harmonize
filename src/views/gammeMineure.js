var m = require("mithril");
var Gamme = require("./gamme");

module.exports = {
    view: function(gammeNode) {
        var gamme = gammeNode.attrs;

        return m('',[
            m("h2", gamme.nom),
            m(Gamme,{accords:gamme.harmony.son3 ,son: 3}),
            m(Gamme,{accords:gamme.harmony.son4 ,son: 4})
        ]);

    }
}
