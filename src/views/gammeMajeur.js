var m = require("mithril");
var Gamme = require("./gamme");

module.exports = {
    view: function(gammeNode) {
        var gamme = gammeNode.attrs;

        return m('.panel',[
            m("h1", gamme.nom+" Majeur"),
            m(Gamme,{accords:gamme.harmony.son3 ,son: 3}),
            m(Gamme,{accords:gamme.harmony.son4 ,son: 4})
        ]);

    }
}
