var m = require("mithril")

module.exports = {
	view: function(gammeNode) {
		var gamme = gammeNode.attrs;

		var accords = gamme.accords.map(function(accord) {
			return m("td.accord", accord);
		});
		return m('',[
			m('h3','Accord à '+gamme.son+' sons'),
			m('table',[
				m('tr.accords',accords),
				m('tr',[
					m('td','I'),
					m('td','II'),
					m('td','III'),
					m('td','IV'),
					m('td','V'),
					m('td','VI'),
					m('td','VII'),
				])

			])

		]);
	}
}



/*
module.exports = {
	oninit: function(vnode) {Gammes.get(vnode.attrs.id)},
    view: function() {

    	var displayContent = [];

    	if(Gammes.current.nom !== undefined){
    		displayContent = [
    		   m("h1", Gammes.current.nom+" "+Gammes.current.tonalite)
    		];

    		if (Gammes.current.tonalite === 'Majeur'){
    			displayContent.push( m("h2", "Accord à 3 sons"));

    			var accords = [];
    			Gammes.harmony.son3.map(function(accord) {
    				accords.push( m("span.accord", accord));
    			});
    			displayContent.push( m(".accords", accords));


    			displayContent.push( m("h2", "Accord à 4 sons"));
    			var accords = [];
    			Gammes.harmony.son4.map(function(accord) {
    				accords.push( m("span.accord", accord));
    			});
    			displayContent.push( m(".accords", accords));
    		}
    		else{

    			displayContent.push( m("h2", "Gamme naturelle"));
    			displayContent.push( m("h3", "Accord à 3 sons"));

    			var accords = [];
    			Gammes.harmony.naturelle.son3.map(function(accord) {
    				accords.push( m("span.accord", accord));
    			});
    			displayContent.push( m(".accords", accords));


    			displayContent.push( m("h3", "Accord à 4 sons"));
    			var accords = [];
    			Gammes.harmony.naturelle.son4.map(function(accord) {
    				accords.push( m("span.accord", accord));
    			});
    			displayContent.push( m(".accords", accords));




    			displayContent.push( m("h2", "Gamme harmonique"));
    			displayContent.push( m("h3", "Accord à 3 sons"));

    			var accords = [];
    			Gammes.harmony.harmonique.son3.map(function(accord) {
    				accords.push( m("span.accord", accord));
    			});
    			displayContent.push( m(".accords", accords));


    			displayContent.push( m("h3", "Accord à 4 sons"));
    			var accords = [];
    			Gammes.harmony.harmonique.son4.map(function(accord) {
    				accords.push( m("span.accord", accord));
    			});
    			displayContent.push( m(".accords", accords));

    			displayContent.push( m("h2", "Gamme mélodique"));
    			displayContent.push( m("h3", "Accord à 3 sons"));

    			var accords = [];
    			Gammes.harmony.melodique.son3.map(function(accord) {
    				accords.push( m("span.accord", accord));
    			});
    			displayContent.push( m(".accords", accords));


    			displayContent.push( m("h3", "Accord à 4 sons"));
    			var accords = [];
    			Gammes.harmony.melodique.son4.map(function(accord) {
    				accords.push( m("span.accord", accord));
    			});
    			displayContent.push( m(".accords", accords));

    		}
    	}


    	if(Object.keys(Gammes.current).length > 0){
	    	displayContent.push(m('h2','Aide'),
		    	m('ul.help',[
		    	        m('li','Accords majeurs 7 (M7 ou ▲)'),
		    	        m('li','Accords 7 (7)'),
		    	        m('li','Accords mineurs 7 (m7 ou -7)'),
		    	        m('li','Accords mineurs 7/5b (m7/5b ou Ø)'),
		    	        m('li','Accords diminués (dim ou °)'),

	    	]));
		}



        return m(".gamme-detail",displayContent );
    }
}
*/
