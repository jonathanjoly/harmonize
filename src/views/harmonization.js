var m = require("mithril");
var Gammes = require("../models/gammes");
var GammeMajeur = require("./gammeMajeur");
var GammesMineures = require("./gammesMineures");


module.exports = {
    view: function() {

        var displayContent = [];

        if(Object.keys(Gammes.current).length > 0){
            if (Gammes.current.tonalite === 'Majeur'){
                displayContent = [
                    m('.bottom-right-content-left',
                        m(GammeMajeur,{
                            nom:Gammes.current.nom ,
                            harmony:Gammes.harmony
                        })
                    ),
                    m('.bottom-right-content-right',
                        m(GammesMineures,{
                            nom:Gammes.relativeGamme.nom ,
                            harmony:Gammes.relativeHarmony
                        })
                    )
                ];
            }
            else{
                displayContent = [
                    m('.bottom-right-content-left',
                        m(GammesMineures,{
                            nom:Gammes.current.nom ,
                            harmony:Gammes.harmony
                        })
                    ),
                    m('.bottom-right-content-right',
                        m(GammeMajeur,{
                            nom:Gammes.relativeGamme.nom ,
                            harmony:Gammes.relativeHarmony
                        })
                    )
                ];
            }
        }

        return m('.harmonization',displayContent);
    }
};
