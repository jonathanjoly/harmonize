var m = require("mithril")
var Gammes = require("../models/gammes")
var GammeDetail = require("./gamme");


module.exports = {
    oninit: Gammes.loadList,

    view: function() {

        // All the options for the select
        var selectOptions = [];

        // The default option for the select
        if(Object.keys(Gammes.current).length === 0){
            selectOptions[0] = m("option.gamme-list-item",{value:'NO'},'Choisissez une gamme');
        }


        // Add the list of gammes
        selectOptions = selectOptions.concat(
            Gammes.list.map(function(gamme) {
                return m("option.gamme-list-item",{value: gamme.id},gamme.nom + " " + gamme.tonalite)
            })
        );

    	return[ m("div.content", m("select.gamme-list",{onchange:function(){
    		var id = this.options[this.selectedIndex].value;
    		Gammes.get(id);
    		Gammes.harmonize();
    	}},selectOptions),
        	m(GammeDetail,{}))
        ]
    }
}
