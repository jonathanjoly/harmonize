var m = require("mithril");
var GammeMineure = require("./gammeMineure");

module.exports = {
    view: function(gammeNode) {
        var gamme = gammeNode.attrs;

        return m('.panel',[
            m("h1", gamme.nom+" Mineur"),
            m(GammeMineure,{
                nom:'Naturelle',
                harmony: gamme.harmony.naturelle
            }),
            m(GammeMineure,{
                nom:'Harmonique',
                harmony: gamme.harmony.harmonique
            }),
            m(GammeMineure,{
                nom:'Mélodique',
                harmony: gamme.harmony.melodique
            })

        ]);

    }
}
