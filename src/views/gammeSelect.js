var m = require("mithril");
var Gammes = require("../models/gammes");

module.exports = {
    oninit: Gammes.loadList, // Get the list of gammes
    view: function() {
        // All the options for the select
        var selectOptions = [];

        // The default option for the select
        if(Object.keys(Gammes.current).length === 0){
            selectOptions[0] = m("option.gamme-list-item",{value:'NO'},'Choisissez une gamme');
        }
        // Add the list of gammes
        selectOptions = selectOptions.concat(
            Gammes.list.map(function(gamme) {
                return m("option.gamme-list-item",{value: gamme.id},gamme.nom + " " + gamme.tonalite)
            })
        );

        return m("select",{onchange:function(){
    		var id = this.options[this.selectedIndex].value;
    		Gammes.setCurrent(id);
    		Gammes.harmonize();
    	}},selectOptions);

    }
};
